{ config, pkgs, lib, ... }:

with lib;

{
  # We allow closed source (unfree) software on this build
  nixpkgs.config.allowUnfree = true;
}

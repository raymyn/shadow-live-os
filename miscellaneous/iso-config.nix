{ config, pkgs, lib, ... }:

with lib;

{
  imports = [
    # We import the required base for a Nix livecd
    # <nixpkgs/nixos/modules/installer/cd-dvd/iso-image.nix>

    # Import a lot of drivers for maximum compatibility
    <nixpkgs/nixos/modules/profiles/all-hardware.nix>

    # Import base channels
    # <nixpkgs/nixos/modules/installer/cd-dvd/channel.nix>
  ];

  # ISO naming.
  isoImage.isoName = "Shadow-LiveOS.iso";
  isoImage.isoBaseName = "shadow";
  isoImage.volumeID = "ShadowOS";

  # Theme
  isoImage.efiSplashImage = ../assets/background.png;
  isoImage.splashImage = ../assets/background.png;
  # isoImage.grub-theme = ;

  # Disable documentation
  documentation.enable = mkForce false;
  documentation.nixos.enable = mkForce false;

  # Tells Nix to garbage collect more aggressively
  environment.variables.GC_INITIAL_HEAP_SIZE = "1M";

  # Stability for low memory systems
  boot.kernel.sysctl."vm.overcommit_memory" = "1";

  # Allow to write EFI variables
  boot.loader.efi.canTouchEfiVariables = false;

  # Configure boot
  boot.loader.grub.memtest86.enable = false;
  boot.loader.timeout = mkForce 0;
}

# Shadow Live OS (NixOS Flavor)

![Size of the ISO](https://img.shields.io/badge/dynamic/json.svg?label=size&query=size&colorB=brightgreen&url=https%3A%2F%2Fgitlab.com%2FNicolasGuilloux%2Fshadow-live-os%2F-%2Fjobs%2Fartifacts%2Fnix-master%2Fraw%2Fbadges.json%3Fjob%3Dshadow)
![Compilation time](https://img.shields.io/badge/dynamic/json.svg?label=compilation%20time&query=execution_time&colorB=brightgreen&url=https%3A%2F%2Fgitlab.com%2FNicolasGuilloux%2Fshadow-live-os%2F-%2Fjobs%2Fartifacts%2Fnix-master%2Fraw%2Fbadges.json%3Fjob%3Dshadow)


Shadow Live OS is a project based on Linux to create a Live OS (an OS loaded in the RAM) that can quickly run Shadow out of the box. The OS should be light and embed all libraries and drivers to be usable on all computers.

This branch is based on NixOS, a functionnal Linux built from configuration files.

You can find a variant based on Arch Linux [here](https://gitlab.com/NicolasGuilloux/shadow-live-os/tree/arch-master).

You can find a variant based on Alpine Linux [here](https://gitlab.com/NicolasGuilloux/shadow-live-os/tree/alpine-master).

A big thanks to Elyhaka who built all the [Shadow packages](https://github.com/Elyhaka/shadow-nix) for Nix and who trained me to use [NixOS](https://nixos.org/). What a great OS :)



## Installation

To install it, make sure the pipeline is not running: [![Pipeline state](https://gitlab.com/NicolasGuilloux/shadow-live-os/badges/nix-master/pipeline.svg)](https://gitlab.com/NicolasGuilloux/shadow-live-os/pipelines)

If everything is okay, you may download your favorite image bellow.

[![Shadow LiveOS](./.gitlab/download_stable.svg)](https://gitlab.com/NicolasGuilloux/shadow-live-os/-/jobs/artifacts/nix-master/raw/Shadow-LiveOS.iso?job=shadow)
[![Shadow Beta LiveOS](./.gitlab/download_beta.svg)](https://gitlab.com/NicolasGuilloux/shadow-live-os/-/jobs/artifacts/nix-master/raw/ShadowBeta-LiveOS.iso?job=shadow-beta)
[![Shadow Alpha LiveOS](./.gitlab/download_alpha.svg)](https://gitlab.com/NicolasGuilloux/shadow-live-os/-/jobs/artifacts/nix-master/raw/ShadowAlpha-LiveOS.iso?job=shadow-alpha)

Once downloaded, flash it on a USB drive with your favorite software. We recommand to use [Etcher](https://www.balena.io/etcher/). Boot on it and enjoy :)



## Usage

You may use the right click on the desktop to access to all the software installed and configure your OS to connect to the WiFi for instance.



## Hack & build

To build the image, you need to have Nix package manager installed and functionnal on your machine. You also need to subscribe to the unstable channel for now, as the Shadow package requires it.

To build it, execute the `build.sh` script or simply execute the following command:

```
nix-build '<nixpkgs/nixos>' -A config.system.build.isoImage -I nixos-config=default.nix
```



## Discord servers

  Find us on Shadow Official Discord servers!

- [Discord Shadow FR](https://discordapp.com/invite/shadowtech)
- [Discord Shadow UK](https://discordapp.com/invite/ShadowEN)
- [Discord Shadow DE](https://discord.gg/shadowde)
- [Discord Shadow US](https://shdw.me/USDiscord)


## Maintainers
![Alex^#1629](https://cdn.discordapp.com/avatars/401575828590428161/36d0ac43c2cb3a72d41c51b0c8375f65.png?size=64 "Alex^#1629")
![GiantPandaRoux#0777](https://cdn.discordapp.com/avatars/267044035032514561/e98147b99f4821c4e806e97fda05e69a.png?size=64 "GiantPandaRoux#0777")
![Nover#9563](https://cdn.discordapp.com/avatars/248726456551604224/4f22c1d6e37874987470c1af7dc21d10.png?size=64 "Nover#9563")
![0x4cc3a96f#4425](https://cdn.discordapp.com/avatars/203814496244858881/170107c22305c7c9dff15cdb87acf274.png?size=64 "0x4cc3a96f#4425")
![Elyhaka](https://cdn.discordapp.com/avatars/467628937883811860/b9c4750ea279cdd7d6db2503290551b4.png?size=64 "Ély#5135")

## Disclaimer

This is a community project, project is not affliated to Blade in any way.

[Shadow](https://shadow.tech) logo and embeded Linux client is property of [Blade Group](http://www.blade-group.com/).

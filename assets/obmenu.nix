{ lib, writeText }:

let
  menuItem = (name: value: ''
    <item label="${name}">
        <action name="Execute">
            <command>${value}</command>
        </action>
    </item>
  '');

  parseMenuItems = items:
    lib.strings.concatStringsSep "\n"
    (lib.attrsets.mapAttrsToList menuItem items);

  # List of keyboard layouts
  keyboardLayout = {
    "QWERTY (UK)" = "setxkbmap -layout gb";
    "QWERTY (US)" = "setxkbmap -layout us";
    "QWERTY (DK)" = "setxkbmap -layout dk";
    "QWERTY (SE)" = "setxkbmap -layout se";
    "QWERTZ (DE)" = "setxkbmap -layout de";
    "QWERTZ (CH-FR)" = "setxkbmap ch fr";
    "AZERTY (FR)" = "setxkbmap -layout fr";
    "DVORAK (US)" = "setxkbmap -layout us -variant dvorak"
    "QWERTY MAC (UK)" = "setxkbmap -layout gb -model macintosh";
    "QWERTY MAC (US)" = "setxkbmap -layout us -model macintosh";
    "QWERTY MAC (DE)" = "setxkbmap -layout de -model macintosh";
    "AZERTY MAC (FR)" = "setxkbmap -layout fr -model macintosh";
  };

  # Shadow launcher variants
  shadowLauncherVariants = {
    "Intel iHD" = "shadow-launcher iHD";
    "Intel i965" = "shadow-launcher i965";
    "AMD amdgpu" = "shadow-launcher amdgpu";
    "AMD radeonsi" = "shadow-launcher radeonsi";
  };

  # Extra softwares
  extraSoftwares = {
    "Network" = "nm-connection-editor";
    "Bluetooth" = "bluemoon";
    "Display" = "lxrandr";
    "Sound" = "pavucontrol";
  };
in writeText "obmenu.xml" ''
    <?xml version="1.0" encoding="UTF-8"?>
    <openbox_menu xmlns="http://openbox.org/3.4/menu">

        <!-- Keyboard -->
        <menu id="options-keyboard-menu" label="Keyboard Layouts">
            ${parseMenuItems keyboardLayout}
        </menu>

        <menu id="root-menu" label="ShadowOS">
            <separator label="Applications" />
            ${menuItem "Shadow" "shadow-launcher"}
            <menu id="driver-launch" label="Start Shadow with specific driver">
                ${parseMenuItems shadowLauncherVariants}
            </menu>

            <separator label="Configurations" />
            ${parseMenuItems extraSoftwares}
            <menu id="options-keyboard-menu"/>

            <separator label="System" />
            ${menuItem "Terminal Alacritty" "alacritty"}
            ${menuItem "Terminal Xterm" "xterm"}

            <separator />

            <item label="Suspend">
              <action name="Execute">
                <command>systemctl suspend</command>
              </action>
            </item>

            <item label="Shutdown">
              <action name="Execute">
                <command>systemctl poweroff</command>
              </action>
            </item>

            <item label="Restart">
              <action name="Execute">
                <command>systemctl reboot</command>
              </action>
            </item>
        </menu>
  </openbox_menu>
''


{ config, pkgs, ... }:

{
  # Provides the `vainfo` command
  environment.systemPackages = with pkgs; [ libva-utils ];

  # Hardware hybrid decoding
  nixpkgs.config.packageOverrides = pkgs: {
    vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
  };

  # We enable OpenGL/vaapi
  hardware.opengl = {
    enable = true;
    extraPackages = with pkgs; [
      libva-full

      # Intel
      vaapiIntel
      intel-media-driver

      # AMD/old NVIDIA
      vaapiVdpau
      libGL
      mesa
      libvdpau-va-gl
    ];
  };

  # Bluetooth
  hardware.bluetooth.enable = true;
  services.blueman.enable = true;
}

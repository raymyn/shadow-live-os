{ config, pkgs, ... }: {
  # Enable sound
  sound.enable = true;

  # Configure PulseAudio
  hardware.pulseaudio = {
    enable = true;
    package = pkgs.pulseaudioFull;
  };

  # Packages
  environment.systemPackages = with pkgs; [
    pavucontrol
    pulseaudioFull
    pasystray
  ];

  # Add indicators at startup
  openbox.autostartLines = [
    # Unmute Sound, set sound 50% and start Sound systray
    "${pkgs.pulseaudio}/bin/pactl set-sink-mute 0 false"
    "${pkgs.pulseaudio}/bin/pactl set-sink-volume 0 50%"
    # "${pkgs.pasystray}/bin/pasystray"
  ];
}

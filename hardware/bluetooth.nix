{ config, pkgs, ... }:

{
  # Bluetooth
  hardware.bluetooth.enable = true;
  services.blueman.enable = true;

  # Add PulseAudio bluetooth module
  hardware.pulseaudio.extraModules = [ pkgs.pulseaudio-modules-bt ];

  # Packages
  environment.systemPackages = with pkgs; [ bluezFull ];
}

{ config, pkgs, ... }: {

  # Keep the Xserver libraries
  environment.noXlibs = false;

  # Enable and configure Xserver
  services.xserver = {
    enable = true;
    layout = "fr";
    xkbOptions = "eurosign:e";
    libinput.enable = true;

    displayManager = {
      lightdm.enable = true;
      lightdm.greeters.mini.enable = true;
      autoLogin.enable = true;
      autoLogin.user = "shadow";
    };

    # Disable all desktop managers
    desktopManager.plasma5.enable = false;
    desktopManager.xterm.enable = false;
  };

  # Loading screen
  boot.plymouth = {
    enable = true;
    logo = ../assets/logo.png;
  };
}

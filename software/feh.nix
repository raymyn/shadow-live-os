{ config, pkgs, shadow-package, ... }:

let background = ../assets/background.png;
in {
  # Packages
  environment.systemPackages = with pkgs; [ feh ];

  # Add Feh at startup
  openbox.autostartLines = [ "${pkgs.feh}/bin/feh --bg-fill ${background}" ];
}

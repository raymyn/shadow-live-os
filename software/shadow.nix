{ config, pkgs, ... }:

let
  shadowPackage = (fetchGit {
    url = "https://github.com/NicolasGuilloux/shadow-nix";
    ref = "v1.0.0";
  });

  # Shadow Binary name based on the channel
  binaryName = "shadow-" + config.programs.shadow-client.channel;

  # Create a derivation for the script
  shadowLauncher = pkgs.writeShellScriptBin "shadow-launcher" ''
    # Kill all Shadow instance
    pkill -9 ${binaryName}
    pkill -9 Shadow

    # Execute Shadow without forcing LibVA driver
    if [ $# -eq 0 ]; then
        echo "Starting Shadow with default VA driver"
        ${binaryName}
        exit;
    fi

    # Execute Shadow forcing LibVA driver
    echo "Starting Shadow with $1 VA driver"
    LIBVA_DRIVER_NAME=$1 ${binaryName};
  '';
in {
  imports = [
    # We import the latest Shadow derivation from Elyhaka repository
    (shadowPackage + "/import/system.nix")
  ];

  # Install the script 
  environment.systemPackages = [ shadowLauncher ];

  # We enable Shadow itself
  programs.shadow-client = {
    enable = true;
    enableDiagnostics = false;
    x-session.enable = true;
  };

  # Enable the Remember Me feature
  services.gnome3.gnome-keyring.enable = true;

  # Add Shadow at startup
  openbox.autostartLines = [ "${binaryName}" ];
}

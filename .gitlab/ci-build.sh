#!/usr/bin/env bash

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
BASE_DIR="$CURRENT_DIR/.."

# Start time
start=`date +%s`

if [ $# -eq 1 ]; then
	export SHADOW_CHANNEL=$1
fi

# Build the ISO
"$BASE_DIR/build.sh"

end=`date +%s`
runtime=$((end-start))
size=`du -sh "$BASE_DIR/result/iso/Shadow-LiveOS.iso" | cut -f1`

# Export the data for the badges
echo "{\"execution_time\":\"$runtime seconds\", \"size\":\"$size\"}" > "$BASE_DIR/badges.json"

# Move ISO and fix permission
cp "$BASE_DIR/result/iso/Shadow-LiveOS.iso" "$BASE_DIR/Shadow-LiveOS.iso"
chmod 750 "$BASE_DIR/Shadow-LiveOS.iso"
